﻿namespace VisualWrapperTester
{
    partial class FrmVisualWrapperTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnGuardarDocumento = new System.Windows.Forms.Button();
            this.btnAgregarKeyword = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nupdTypeId = new System.Windows.Forms.NumericUpDown();
            this.txtDocumentTypeName = new System.Windows.Forms.TextBox();
            this.gvKeywords = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupdTypeId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvKeywords)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 331);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.numericUpDown1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(565, 305);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "EliminarDocumentos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(233, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "EliminarDocumento";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(112, 22);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(76, 20);
            this.numericUpDown1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "DocumentHandler:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(565, 305);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "GuardarDocumentos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnGuardarDocumento);
            this.splitContainer1.Panel1.Controls.Add(this.btnAgregarKeyword);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.nupdTypeId);
            this.splitContainer1.Panel1.Controls.Add(this.txtDocumentTypeName);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gvKeywords);
            this.splitContainer1.Size = new System.Drawing.Size(559, 299);
            this.splitContainer1.SplitterDistance = 71;
            this.splitContainer1.TabIndex = 5;
            // 
            // btnGuardarDocumento
            // 
            this.btnGuardarDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardarDocumento.Location = new System.Drawing.Point(340, 35);
            this.btnGuardarDocumento.Name = "btnGuardarDocumento";
            this.btnGuardarDocumento.Size = new System.Drawing.Size(205, 23);
            this.btnGuardarDocumento.TabIndex = 4;
            this.btnGuardarDocumento.Text = "Guardar Documento";
            this.btnGuardarDocumento.UseVisualStyleBackColor = true;
            // 
            // btnAgregarKeyword
            // 
            this.btnAgregarKeyword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAgregarKeyword.Location = new System.Drawing.Point(442, 5);
            this.btnAgregarKeyword.Name = "btnAgregarKeyword";
            this.btnAgregarKeyword.Size = new System.Drawing.Size(103, 23);
            this.btnAgregarKeyword.TabIndex = 4;
            this.btnAgregarKeyword.Text = "Agregar Keyword";
            this.btnAgregarKeyword.UseVisualStyleBackColor = true;
            this.btnAgregarKeyword.Click += new System.EventHandler(this.btnAgregarKeyword_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DocumentTypeId:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "DocumentTypeName:";
            // 
            // nupdTypeId
            // 
            this.nupdTypeId.Location = new System.Drawing.Point(103, 8);
            this.nupdTypeId.Name = "nupdTypeId";
            this.nupdTypeId.Size = new System.Drawing.Size(96, 20);
            this.nupdTypeId.TabIndex = 3;
            // 
            // txtDocumentTypeName
            // 
            this.txtDocumentTypeName.Location = new System.Drawing.Point(122, 35);
            this.txtDocumentTypeName.Name = "txtDocumentTypeName";
            this.txtDocumentTypeName.Size = new System.Drawing.Size(145, 20);
            this.txtDocumentTypeName.TabIndex = 2;
            // 
            // gvKeywords
            // 
            this.gvKeywords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvKeywords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvKeywords.Location = new System.Drawing.Point(0, 0);
            this.gvKeywords.Name = "gvKeywords";
            this.gvKeywords.Size = new System.Drawing.Size(559, 224);
            this.gvKeywords.TabIndex = 0;
            // 
            // FrmVisualWrapperTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 331);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmVisualWrapperTester";
            this.Text = "WrapperTester";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nupdTypeId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvKeywords)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDocumentTypeName;
        private System.Windows.Forms.NumericUpDown nupdTypeId;
        private System.Windows.Forms.Button btnGuardarDocumento;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnAgregarKeyword;
        private System.Windows.Forms.DataGridView gvKeywords;
    }
}

