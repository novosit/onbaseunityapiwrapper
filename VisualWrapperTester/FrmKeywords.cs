﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnityApiWrapper.Models;

namespace VisualWrapperTester
{
    public partial class FrmKeywords : Form
    {
        public KeywordModel keywordTyped;

        public FrmKeywords()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKeywordName.Text) || string.IsNullOrEmpty(txtKeywordValue.Text))
            {
                MessageBox.Show("No se pueden dejar informaciones vacias. Verifique por favor!");
            }
            else
            {
                keywordTyped = new KeywordModel(txtKeywordName.Text, txtKeywordValue.Text);
            }

            this.Close();
        }
    }
}
