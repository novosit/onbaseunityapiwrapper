﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnityApiWrapper.Models;

namespace VisualWrapperTester
{
    public partial class FrmVisualWrapperTester : Form
    {
        List<KeywordModel> keywords = new List<KeywordModel>();

        public FrmVisualWrapperTester()
        {
            InitializeComponent();
        }

        private void btnAgregarKeyword_Click(object sender, EventArgs e)
        {
            FrmKeywords frmKeywords = new FrmKeywords();
            frmKeywords.ShowDialog();

            keywords.Add(frmKeywords.keywordTyped);

            gvKeywords.DataSource = null;
            gvKeywords.DataSource = keywords;
        }
    }
}
