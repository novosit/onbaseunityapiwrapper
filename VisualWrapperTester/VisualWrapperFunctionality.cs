﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityApiWrapper.Models;

namespace VisualWrapperTester
{
    public class VisualWrapperFunctionality
    {
        List<KeywordModel> _keywords = new List<KeywordModel>();
        DocumentTypeModel _documentType;

        public List<KeywordModel> Keywords { get { return _keywords; } }

        public void AddKeyword(string typeName, string value)
        {
            _keywords.Add(new KeywordModel() { TypeName = typeName, Value = value });
        }

        public void SetDocumentType(string typeName, int typeId)
        {
            _documentType = new DocumentTypeModel() { Id = typeId, Name= typeName };
        }
        public void GuardarDocumento()
        {


        }


    }
}
