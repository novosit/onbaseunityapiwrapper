﻿using System.Configuration;
using UnityApiWrapper.Models.Contracts;

namespace UnityApiWrapper.Models.Implementations
{
    public sealed class KeywordsConfiguration: IKeywordsConfiguration
    {
        public string GetKeywordVigenciaName()
        {
            return ConfigurationManager.AppSettings["keywordVigencia"];
        }

        public string GetKeywordFechaExpiracion()
        {
            return ConfigurationManager.AppSettings["keywordFechaExpiracion"];
        }

        public string GetKeywordNumeroCliente()
        {
            return ConfigurationManager.AppSettings["keywordNumeroCliente"];
        }

        public string GetKeywordCedulaRNCPasaporte()
        {
            return ConfigurationManager.AppSettings["keywordCedulaRNCPasaporte"];
        }
    }
}
