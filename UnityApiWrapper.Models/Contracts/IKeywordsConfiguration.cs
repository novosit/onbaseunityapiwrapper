﻿namespace UnityApiWrapper.Models.Contracts
{
    public interface IKeywordsConfiguration
    {
        string GetKeywordVigenciaName();
        string GetKeywordFechaExpiracion();
        string GetKeywordNumeroCliente();
        string GetKeywordCedulaRNCPasaporte();
    }
}
