﻿using System;

namespace UnityApiWrapper.Models.Exceptions
{
    /// <summary>
    /// Representa la excepción que agrupa las excepciones de UnityAPI relacionados con Licencia.
    /// </summary>
    [Serializable]
    public class UnitApiLicenseException : ArgumentException
    {
        /// <inheritdoc />
        public UnitApiLicenseException()
        {

        }

        /// <inheritdoc />
        public UnitApiLicenseException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public UnitApiLicenseException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
