﻿using System;

namespace UnityApiWrapper.Models.Exceptions
{
    /// <summary>
    /// Representa la excepción por los tipos de documentos de onbase invalidos u no existentes.
    /// </summary>
    [Serializable]
    public sealed class InvalidDocumentTypeException : ArgumentException
    {
        /// <inheritdoc />
        public InvalidDocumentTypeException()
        {

        }

        /// <inheritdoc />
        public InvalidDocumentTypeException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public InvalidDocumentTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
