﻿using System;

namespace UnityApiWrapper.Models.Exceptions
{
    /// <summary>
    /// Representa la excepción por los tipos de keywords de onbase invalidos u no existentes.
    /// </summary>
    [Serializable]
    public sealed class InvalidKeywordTypeException : ArgumentException
    {
        /// <inheritdoc />
        public InvalidKeywordTypeException()
        {

        }

        /// <inheritdoc />
        public InvalidKeywordTypeException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public InvalidKeywordTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }


}
