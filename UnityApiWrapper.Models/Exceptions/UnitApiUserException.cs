﻿using System;

namespace UnityApiWrapper.Models.Exceptions
{
    /// <summary>
    /// Representa la excepción que agrupa las excepciones de UnityAPI relacionados con problemas de Usuario.
    /// </summary>
    [Serializable]
    public class UnityApiUserException : ArgumentException
    {
        /// <inheritdoc />
        public UnityApiUserException()
        {

        }

        /// <inheritdoc />
        public UnityApiUserException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public UnityApiUserException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
