﻿using System;

namespace UnityApiWrapper.Models.Exceptions
{
    /// <summary>
    /// Representa la excepción que agrupa las excepciones de UnityAPI general.
    /// </summary>
    [Serializable]
    public class UnityApiGeneralException : ArgumentException
    {
        /// <inheritdoc />
        public UnityApiGeneralException()
        {

        }

        /// <inheritdoc />
        public UnityApiGeneralException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public UnityApiGeneralException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
