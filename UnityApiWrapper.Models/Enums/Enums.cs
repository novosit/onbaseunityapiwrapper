﻿
namespace UnityApiWrapper.Models.Enums
{
    /// <summary>
    /// Representan los tipos de formatos implementados
    /// </summary>
    public enum FormatType
    {
        /// <summary>
        /// Indica el formato PDF
        /// </summary>
        PDF = 1,

        /// <summary>
        /// Indica el formato imagen
        /// </summary>
        Image = 0
    }

    /// <summary>
    /// Representan los tipos de licencia para conectarse a onbase
    /// </summary>
    ///     
    public enum LicenseConnectionType
    {
        /// <summary>
        /// Indica ningun tipo de licencia
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Se refiere al uso del tipo de licencia Query Hour
        /// </summary>
        QUERY_HOUR = 1,

        /// <summary>
        /// Se refiere al uso del tipo de licencia nombradas
        /// </summary>
        NAMED_LICENSE = 2
    }
}
