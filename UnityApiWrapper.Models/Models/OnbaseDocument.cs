﻿using System.Collections.Generic;

namespace UnityApiWrapper.Models.Models
{
    /// <summary>
    /// Modelo que representa documentos de Onbase
    /// </summary>
    public class OnbaseDocument
    {
        /// <summary>
        /// Identificador del tipo de documento
        /// </summary>
        public long DocumentTypeId { get; set; }
        /// <summary>
        /// Nombre del tipo de documento
        /// </summary>
        public string DocumentTypeName { get; set; }
        /// <summary>
        /// Identificador del documento
        /// </summary>
        public long DocumentHandler { get; set; }
        /// <summary>
        /// Lista de llaves asociadas al documento
        /// </summary>
        public List<KeywordModel> Keywords { get; set; }

        /// <summary>
        /// Representa la representacion base64 de la imagen alojada en Onbase
        /// </summary>
        public DocumentBinary Image { get; set; }

        /// <summary>
        /// Permite obtener un keyword dado su nombre
        /// </summary>
        /// <param name="name">Nombre del keyword</param>
        /// <returns></returns>
        public KeywordModel GetKeyword(string name)
        {
            if (Keywords.Count == 0)
            {
                return null;
            }

            return new KeywordModel();
        }

        /// <summary>
        /// Permite obtener un keyword dado el id de su tipo
        /// </summary>
        /// <param name="keywordTypeId"></param>
        /// <returns></returns>
        public KeywordModel GetKeyword(int keywordTypeId)
        {
            if (Keywords.Count == 0)
            {
                return null;
            }

            return new KeywordModel();
        }
    }
}
