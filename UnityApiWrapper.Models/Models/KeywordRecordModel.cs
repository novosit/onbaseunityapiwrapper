﻿using System.Collections.Generic;

namespace UnityApiWrapper.Models.Models
{
    public class KeywordRecordModel
    {
        public string Name { get; }

        public List<KeywordModel> Keywords { get; }

        public KeywordRecordModel(string keywordRecordName, List<KeywordModel> keywords)
        {
            Name = keywordRecordName;
            Keywords = keywords;
        }
    }
}
