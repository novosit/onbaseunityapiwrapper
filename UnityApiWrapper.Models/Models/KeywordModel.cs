﻿
namespace UnityApiWrapper.Models
{
    /// <summary>
    /// Representa la información de un Keyword de Onbase
    /// </summary>

    public class KeywordModel
    {
        /// <summary>
        /// Inicializa una instancia de la clase Models.KeywordModel
        /// </summary>
        public KeywordModel()
        {

        }

        /// <summary>
        /// Inicializa un keyword con un tipo y valor dados
        /// </summary>
        /// <param name="type">Tipo del keyword</param>
        /// <param name="value">Valor del keyword</param>
        public KeywordModel(string type, string value)
        {
            TypeName = type;
            this.Value = value;
        }

        /// <summary>
        /// Inicializa una instancia de la clase Models.KeywordModel
        /// </summary>
        public KeywordModel(string type, string value, string keywordTypeGroup)
        {
            TypeName = type;
            Value = value;
            KeywordTypeGroup = keywordTypeGroup;
        }

        /// <summary>
        /// Nombre del tipo de keyword
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Valor del keyword
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Valor del grupo de keyword
        /// </summary>
        public string KeywordTypeGroup { get; set; }
    }
}
