﻿
namespace UnityApiWrapper.Models
{
    /// <summary>
    /// Represents a model for a Onbase DocumentType.
    /// </summary> 
    public sealed class DocumentTypeModel
    {
        /// <summary>
        /// Indica el id del tipo Documento de Onbase
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Indica el nombre del tipo de documento
        /// </summary>
        public string Name { get; set; }
    }
}
