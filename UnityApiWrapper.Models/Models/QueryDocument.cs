﻿using System.Runtime.Serialization;

namespace UnityApiWrapper.Models
{
    /// <summary>
    /// Representa un conjunto de informacion de consulta para los documentos de Onbase
    /// </summary>
    [DataContract]
    
    public class QueryDocument
    {
        /// <summary>
        /// Id del documento en Onbase.
        /// </summary>
        [DataMember]
        public string DocumentHandler { get; set; }

        /// <summary>
        /// Muestra el valor del Keyword Cedula/Rnc.
        /// </summary>
        [DataMember]
        public string Cedula_Pasaporte { get; set; }

        /// <summary>
        /// Contiene el valor del keyword Codigo cliente.
        /// </summary>
        [DataMember]
        public string CodigoCliente { get; set; }

        /// <summary>
        /// Contiene el id del tipo de documento.
        /// </summary>
        [DataMember]
        public string DocumentTypeId { get; set; }

        /// <summary>
        /// Contiene el nombre del tipo de documento.
        /// </summary>
        [DataMember]
        public string DocumentTypeName { get; set; }

        /// <summary>
        /// Contiene la representacion base64 del archivo presente en onbase.
        /// </summary>
        [DataMember]
        public string ImageInBase64StringFormat { get; set; }

        /// <summary>
        /// Indica la extension del Documento.
        /// </summary>
        [DataMember]
        public string FileExtension { get; set; }
    }
}
