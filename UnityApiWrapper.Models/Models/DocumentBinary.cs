﻿
namespace UnityApiWrapper.Models.Models
{
    /// <summary>
    /// Representa el archivo binario de un documento
    /// </summary>
    public sealed class DocumentBinary
    {
        /// <summary>
        /// Indica la extension del documento
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Indica la representacion Base64String del binario.
        /// </summary>
        public string Base64String { get; set; }
    }
}
