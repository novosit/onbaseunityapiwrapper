﻿
namespace UnityApiWrapper.Models
{
    /// <summary>
    /// Representación un conjunto de información que muestra si un documento existe en Onbase
    /// </summary>
    public class ValidationDocument
    {
        /// <summary>
        /// Identificador del Documento en Onbase
        /// </summary>
        public string DocumentHandler { get; set; }

        /// <summary>
        /// Indica el Tipo de Documento
        /// </summary>
        public string DocumentTypeId { get; set; }

        /// <summary>
        /// Indica la vigencia del documento.
        /// </summary>
        public string Vigencia { get; set; }

        /// <summary>
        /// Indica la fecha de expiracion del documento si la tiene.
        /// </summary>
        public string FechaExpiracion { get; set; }

        /// <summary>
        /// Indica si el documento existe.
        /// </summary>
        public bool Existe { get; set; }
    }
}
