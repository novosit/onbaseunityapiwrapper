﻿using System.Configuration;

namespace UnityApiWrapper.IntegrationTest
{
    public class AppConfigOnbaseCredentials : BusinessLogic.Contracts.IOnbaseCredentials
    {
        public string GetAppServerUrl()
        {
            return ConfigurationManager.AppSettings["OnbaseAppServerUrl"];
        }

        public string GetOnbaseDatasource()
        {
            return ConfigurationManager.AppSettings["OnbaseDatasource"];
        }

        public string GetOnbaseUserName()
        {
            return ConfigurationManager.AppSettings["OnbaseUserName"];
        }

        public string GetOnbaseUserPassword()
        {
            return ConfigurationManager.AppSettings["OnbaseUserPassword"];
        }
    }
}
