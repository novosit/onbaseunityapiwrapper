﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityApiWrapper.BusinessLogic;
using UnityApiWrapper.Models;
using UnityApiWrapper.Models.Models;

namespace UnityApiWrapper.IntegrationTest
{
    class Program
    {
        /// <summary>
        /// Muestra ejemplos de como utilizar los metodos del UnityApiWrapper
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                GuardarUnityFormWithKeywordRecord();

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Se produjo un error:");
                Console.WriteLine(ex.ToString());
            }

            Console.ReadKey();

        }

        private static void EliminarDocumentos()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            try
            {
                man.EliminarDocumento(19181915);
            }
            finally
            {
                man.CerrarConexion();
            }
        }

        private static void BuscarDocumentos()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            List<KeywordModel> keywords = new List<KeywordModel> { new KeywordModel() { TypeName = "Cuenta #", Value = "12345" } };

            List<DocumentTypeModel> documentTypes = new List<DocumentTypeModel> { new DocumentTypeModel() { Id = 386, Name = "firmas" } };

            try
            {
                var documentosConsultados = man.BuscarDocumentos(documentTypes, keywords);

                foreach (var documento in documentosConsultados)
                {
                    Console.WriteLine("Tipo documento:{0} | documentHandler:{1} ", documento.DocumentTypeName, documento.DocumentHandler);
                }
            }
            finally
            {
                man.CerrarConexion();
            }
        }

        private static void GuardarDocumentos()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            int documentTypeFactura = 386;
            DocumentTypeModel docType = new DocumentTypeModel() { Id = documentTypeFactura, Name = "firmas" };

            List<KeywordModel> keywords = new List<KeywordModel> {
                new KeywordModel() { TypeName = "Cuenta #", Value = "252525" },
                 new KeywordModel() { TypeName = "Nombre Cliente 1", Value = "Prueba 1" },
                  new KeywordModel() { TypeName = "Nombre Cliente 2", Value = "Prueba 2" },
                   new KeywordModel() { TypeName = "Secuencia", Value = "2828" },

            };

            byte[] page = File.ReadAllBytes(@"D:\Archivos X9\Tarjeta de Firma\4417\40000196.TIF");

            string imageInBase64 = Convert.ToBase64String(page);

            try
            {
                man.GuardarDocumentos(docType, imageInBase64, keywords);
            }
            finally
            {
                man.CerrarConexion();
            }

            Console.WriteLine("Documento guardado exitosamente");
        }

        private static void GuardarUnityForm()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            
            List<KeywordModel> keywords = new List<KeywordModel> {
                new KeywordModel() { TypeName = "Numero de tramitacion del Portal", Value = "SDG-PORTAL-001" },
                 new KeywordModel() { TypeName = "Tipologia", Value = "VIVIENDAS" },
                       new KeywordModel() { TypeName = "Nombre del Proyecto", Value = "PROYECTO PRUEBA" },
                             new KeywordModel() { TypeName = "Tecnico Participante Nombre", Value = "Raul Garabito", KeywordTypeGroup="Tecnicos Participantes del Proyecto" },

            };


            DocumentTypeModel docType = new DocumentTypeModel() { Id = 0, Name = "SOLICITUD DE LICENCIA" };

            try
            {
                man.GuardarUnityForm(docType, keywords);
            }
            finally
            {
                man.CerrarConexion();
            }

            Console.WriteLine("Documento guardado exitosamente");
        }

        private static void GuardarUnityFormWithKeywordRecord()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            List<KeywordModel> keywords = new List<KeywordModel> {
                new KeywordModel() { TypeName = "Numero de tramitacion del Portal", Value = "SDG-PORTAL-001" },
                 new KeywordModel() { TypeName = "Tipologia", Value = "VIVIENDAS" },
                       new KeywordModel() { TypeName = "Nombre del Proyecto", Value = "PROYECTO PRUEBA" }

            };

            List<KeywordModel> keywordsInKeywordRecord = new List<KeywordModel> {
                                      new KeywordModel() { TypeName = "Tecnico Participante Nombre", Value = "Raul Garabito"},

            };


            List<KeywordRecordModel> keyRecords = new List<KeywordRecordModel>();

            KeywordRecordModel keywordRecordModel = new KeywordRecordModel("Tecnicos Participantes del Proyecto", keywordsInKeywordRecord);
            keyRecords.Add(keywordRecordModel);

            DocumentTypeModel docType = new DocumentTypeModel() { Id = 0, Name = "SOLICITUD DE LICENCIA" };

            try
            {
                man.GuardarUnityForm(docType, keywords, keyRecords);
            }
            finally
            {
                man.CerrarConexion();
            }

            Console.WriteLine("Documento guardado exitosamente");
        }

        private static void GuardarDocumentosConPaginas()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            int documentTypeFactura = 386;
            DocumentTypeModel docType = new DocumentTypeModel() { Id = documentTypeFactura, Name = "firmas" };

            List<KeywordModel> keywords = new List<KeywordModel> {
                new KeywordModel() { TypeName = "Cuenta #", Value = "252525" },
                 new KeywordModel() { TypeName = "Nombre Cliente 1", Value = "Prueba 1" },
                  new KeywordModel() { TypeName = "Nombre Cliente 2", Value = "Prueba 2" },
                   new KeywordModel() { TypeName = "Secuencia", Value = "2828" },

            };

            List<string> pagesInBase64 = new List<string>();

            byte[] page = File.ReadAllBytes(@"C:\Users\Administrator\Documents\imagenes firma\100001063_MergeFrenteDorso.TIF");

            string imageInBase64 = Convert.ToBase64String(page);

            pagesInBase64.Add(item: imageInBase64);
            pagesInBase64.Add(item: imageInBase64);

            try
            {
                man.GuardarDocumentoConPaginas(docType, pagesInBase64, keywords);
            }
            finally
            {
                man.CerrarConexion();
            }

            Console.WriteLine("Documento con paginas guardado exitosamente");
        }

        private static void ActualizarListaDeKeywords()
        {

            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            int documentTypeFactura = 386;

            DocumentTypeModel docType = new DocumentTypeModel() { Id = documentTypeFactura, Name = "Pagaré" };

            //List<KeywordModel> keywords = new List<KeywordModel> {
            //    new KeywordModel() { TypeName = "Cuenta #", Value = "12345" },
            //     new KeywordModel() { TypeName = "Nombre Cliente 1", Value = "Prueba Demo1" },
            //      new KeywordModel() { TypeName = "Nombre Cliente 2", Value = "Prueba Demo1" },
            //       new KeywordModel() { TypeName = "Secuencia", Value = "1515" }
            //       //new KeywordModel() { TypeName = "Date Completed", Value = "12/24/2017" }

            //};

            List<KeywordModel> keywords = new List<KeywordModel> {
                new KeywordModel() { TypeName = "FT-Evento", Value = "prueba1" },
                 new KeywordModel() { TypeName = "FT-Fecha", Value = "11/23/2017" },
                  new KeywordModel() { TypeName = "FT-Lugar", Value = "AGORA MALL" },

                   new KeywordModel() { TypeName = "FT-Usuario", Value = "ElMayorClasico" }

            };



            try
            {
                string docHandler = "242660627";

                man.ActualizarKeywords(docHandler, keywords);

                Console.WriteLine("Documento actualizado exitosamente");

            }
            finally
            {
                man.CerrarConexion();
            }


        }

        private static void BuscarDocumentosPorDocumentHandler()
        {
            AppConfigOnbaseCredentials credencialesOnbase = new AppConfigOnbaseCredentials();
            IntegrationManager man = new IntegrationManager(credencialesOnbase, Models.Enums.LicenseConnectionType.NAMED_LICENSE, autoInitConnection: true);

            List<string> documentHandlers = new List<string> { "6143" };
            var documents = man.BuscarDocumentosPorDocumentHandler(documentHandlers);
            var keywords = documents.First().Keywords;

            foreach (var item in keywords)
            {
                Console.WriteLine("TIPO:{0} \n valor:{1}", item.TypeName, item.Value);
            }

            man.CerrarConexion();
        }

        private static byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }
            try
            {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
