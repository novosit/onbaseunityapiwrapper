﻿using Hyland.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Json;
using UnityApiWrapper.Models;
using UnityApiWrapper.Models.Models;

namespace UnityApiWrapper.OnbaseDataAccess
{
    /// <summary>
    /// Representa funcionalidades para facilitar el trabajo con los keywords de Onbase
    /// </summary>
    public static class KeywordUtils
    {
        /// <summary>
        /// Devuelve un Keyword de onbase dado su keywordType
        /// </summary>
        /// <param name="keytype"> Tipo de keyword indicado</param>
        /// <param name="value"> Valor del keyword</param>
        /// <returns> Un keyword de onbase del tipo indicado</returns>
        public static Keyword ConvertToOnbaseKeyword(KeywordType keytype, string value)
        {
            Keyword key = null;
            switch (keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(value);
                    key = keytype.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:

                    DateTime dateVal = DateTime.Parse(value);
                    key = keytype.CreateKeyword(dateVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(value);
                    key = keytype.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(value);
                    key = keytype.CreateKeyword(lngVal);
                    break;
                default:
                    key = keytype.CreateKeyword(value);
                    break;
            }
            return key;
        }

        /// <summary>
        /// Convierte una lista de keywords indicada por una lista de keywords de onbase
        /// </summary>
        /// <param name="keywords">Keywords a convertir</param>
        /// <param name="currentApp">Componente con la conexion activa hacia onbase</param>
        /// <returns></returns>
        public static List<Keyword> ConvertToOnbaseKeywordList(IEnumerable<KeywordModel> keywords, Application currentApp)
        {
            currentApp.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
            List<Keyword> result = new List<Keyword>();

            foreach (KeywordModel keyword in keywords)
            {
                KeywordType keywordType = FindKeywordTypeByName(keyword.TypeName, currentApp);

                LogToEventViewer(string.Format("keyword that will be converted:{0}", GetJson(keyword.GetType(), keyword)));
                Keyword newKeyword = ConvertToOnbaseKeyword(keywordType, keyword.Value);
              
                result.Add(newKeyword);
            }

            return result;
        }

        private static void LogToEventViewer(string message)
        {
            // Create an EventLog instance and assign its source.
            EventLog myLog = new EventLog { Source = "Application" };
            myLog.WriteEntry(message, EventLogEntryType.SuccessAudit, 555);
        }

        private static string GetJson(Type type, object obje)
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(type);

            ser.WriteObject(stream1, obje);
            stream1.Position = 0;
            string result = "";

            using (StreamReader sr = new StreamReader(stream1))
            {
                result = sr.ReadToEnd();
            }

            return result;

        }

        public static List<EditableKeywordRecord> ConvertToOnbaseKeywordRecord(List<KeywordRecordModel> keywordRecords, Application currentApp)
        {
            List<EditableKeywordRecord> result = new List<EditableKeywordRecord>();

            foreach (var keyRecord in keywordRecords)
            {
                KeywordRecordType keyRecType = currentApp.Core.KeywordRecordTypes.Find(keyRecord.Name);

                if (keyRecType == null)
                {
                    throw new Exception(string.Format("KeywordRecord: [{0}] not found!", keyRecord.Name));
                }

                List<Keyword> keywords = ConvertToOnbaseKeywordList(keyRecord.Keywords, currentApp);

                EditableKeywordRecord editableKeywordRecord = keyRecType.CreateEditableKeywordRecord();

                foreach (var key in keywords)
                {
                    editableKeywordRecord.AddKeyword(key);
                }

                result.Add(editableKeywordRecord);

            }

            return result;
        }

        /// <summary>
        /// Obtiene el valor default de un keyword vacio
        /// </summary>
        /// <param name="keytype">Tipo de keyword de onbase</param>
        /// <returns></returns>
        public static string GetDefaultKeywordValue(KeywordType keytype)
        {
            // Keyword key = null;
            switch (keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    //decimal decVal = decimal.Parse(value);
                    // key = keytype.CreateKeyword(decVal);
                    return "0";
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    return "1/1/2010";
                case KeywordDataType.FloatingPoint:
                    return "0";
                case KeywordDataType.Numeric9:
                    return "0";
                default:
                    return "0";
            }

        }

        /// <summary>
        /// Devuelve el tipo de keyword para nombre dado
        /// </summary>
        /// <param name="keyTypeName"> Nombre del keyword de Onbase </param>
        /// <returns> Una representacion del tipo de keyword de onbase</returns>
        public static KeywordType FindKeywordTypeByName(string keyTypeName, Application currentApp)
        {
            currentApp.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
            KeywordType keyType;
            try
            {
                keyType = currentApp.Core.KeywordTypes.Find(keyTypeName);
            }
            catch (Exception ex)
            {
                currentApp.Diagnostics.Write(ex.ToString());
                keyType = null;
            }

            return keyType;
        }

        /// <summary>
        /// Devuelve la lista de keywords de un documento dado
        /// </summary>
        /// <param name="documento"> Documento dado</param>
        /// <returns>Lista de keywords</returns>
        public static List<Keyword> GetDocumentKeywordList(Document documento)
        {
            List<Keyword> keywords = new List<Keyword>();

            if (documento == null)
                return keywords;

            var keywordRecord = documento.KeywordRecords;

            foreach (var keyword in keywordRecord)
            {
                var listKeyword = keyword.Keywords;
                keywords.AddRange(listKeyword);
            }

            return keywords;
        }


    }
}
