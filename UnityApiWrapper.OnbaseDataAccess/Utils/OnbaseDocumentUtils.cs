﻿using Hyland.Unity;
using System;
using System.Collections.Generic;
using UnityApiWrapper.Models;

namespace UnityApiWrapper.OnbaseDataAccess
{
    public static class OnbaseDocumentUtils
    {
        /// <summary>
        /// Busca tipo de documento por nombre
        /// </summary>
        /// <param name="DocumentTypeName"> Nombre del tipo de Documento</param>
        /// <returns></returns>
        public static DocumentType FindDocumentTypeByName(string DocumentTypeName, Application currentApp)
        {
            currentApp.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
            DocumentType type;

            try
            {
                type = currentApp.Core.DocumentTypes.Find(DocumentTypeName);
            }
            catch (Exception ex)
            {
                currentApp.Diagnostics.Write(ex.ToString());
                type = null;
            }

            return type;
        }

        /// <summary>
        /// Busca tipo de documento por Id
        /// </summary>
        /// <param name="DocumentTypeId"> Idtipo de Documento</param>
        /// <returns>Un tipo de documento de Onbase</returns>
        public static DocumentType FindDocumentTypeById(int DocumentTypeId, Application currentApp)
        {
            currentApp.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
            DocumentType type;
            long id = Convert.ToInt64(DocumentTypeId);
            try
            {
                type = currentApp.Core.DocumentTypes.Find(id);
            }
            catch (Exception ex)
            {
                currentApp.Diagnostics.Write(ex.ToString());
                type = null;
            }

            return type;
        }

        /// <summary>
        /// Convierte una lista de documentTypeModel en DocumentTypes de Onbase
        /// </summary>
        /// <param name="documentTypes">Lista de tipos de documento</param>
        /// <param name="currentApp">Componente con la conexion activa hacia onbase</param>
        /// <returns>Una lista de tipos de documentos de Onbase</returns>
        public static List<DocumentType> ConvertToOnbaseDocumentTypeList(List<DocumentTypeModel> documentTypes, Application currentApp)
        {
            List<DocumentType> result = new List<DocumentType>();

            foreach (var documentType in documentTypes)
            {
                DocumentType keyType = OnbaseDocumentUtils.FindDocumentTypeByName(documentType.Name, currentApp);
                result.Add(keyType);
            }
            return result;
        }
    }
}
