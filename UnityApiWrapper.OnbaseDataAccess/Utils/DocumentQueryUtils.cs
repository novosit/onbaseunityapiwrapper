﻿using Hyland.Unity;
using System.Collections.Generic;

namespace UnityApiWrapper.OnbaseDataAccess
{
    /// <summary>
    /// Representan funciones de ayuda para trabajar con las Consultas de Documentos de Onbase
    /// </summary>
    public static class DocumentQueryUtils
    {
        /// <summary>
        /// Agrega un conjunto de keywords a un componente de criterio busqueda de documento dado
        /// </summary>
        /// <param name="docQuery">Representan los criterios de busqueda para buscar en Onbase</param>
        /// <param name="filteringKeywords">Keywords a agregar al criterio de busqueda</param>
        public static void AddKeywordTypeListToQuery(DocumentQuery docQuery, List<Keyword> filteringKeywords)
        {
            foreach (var keyword in filteringKeywords)
                docQuery.AddKeyword(keyword);

        }

        /// <summary>
        /// Agrega uno o mas tipos de documentos de busqueda a un componente de criterio de busqueda de documento dado 
        /// </summary>
        /// <param name="docQuery">Representan los criterios de busqueda para buscar en Onbase</param>
        /// <param name="documentTypes">Tipos de documentos a agregar al criterio de busqueda</param>
        public static void AddDocumentTypeListToQuery(DocumentQuery docQuery, List<DocumentType> documentTypes)
        {
            foreach (var documentType in documentTypes)
            {
                docQuery.AddDocumentType(documentType);
            }
        }

        /// <summary>
        /// Agrega uno o mas keywords para ser ignorados a un componente de criterio busqueda de documento dado
        /// </summary>
        /// <param name="docQuery">Representan los criterios de busqueda para buscar en Onbase</param>
        /// <param name="ignoringKeywords">Keywords a ignorar al criterio de busqueda</param>
        public static void AddKeywordValuesToIgnore(DocumentQuery docQuery, List<Keyword> ignoringKeywords)
        {
            foreach (var keyword in ignoringKeywords)
                docQuery.AddKeyword(keyword, KeywordOperator.NotEqual, KeywordRelation.And);
        }
    }
}
