﻿using Hyland.Unity;
using Hyland.Unity.UnityForm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityApiWrapper.Models.Exceptions;

namespace UnityApiWrapper.OnbaseDataAccess
{
    /// <summary>
    /// Representa un componente de acceso de datos hacia Onbase utilizando UnityApi
    /// </summary>
    public class UnityApiDataAccess
    {
        private Hyland.Unity.Application _app = null;

        public UnityApiDataAccess(Hyland.Unity.Application app)
        {
            _app = app;
        }

        /// <summary>
        /// Obtiene una lista de Documento de OnBase que cumpla con los criterios indicados.
        /// </summary>
        /// <param name="documentTypes"> Tipos de documento a buscar</param>
        /// <param name="filteringKeywords">Lista de keywords para filtrar</param>
        /// <returns></returns>
        public DocumentList GetOnBaseDocumentGivenDocumentTypesAndKeywords(List<DocumentType> documentTypes, List<Keyword> filteringKeywords)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

            DocumentList documents = null;
            DocumentQuery docQuery = _app.Core.CreateDocumentQuery();
            docQuery.RetrievalOptions = DocumentRetrievalOptions.None;

            try
            {
                DocumentQueryUtils.AddKeywordTypeListToQuery(docQuery, filteringKeywords);

                DocumentQueryUtils.AddDocumentTypeListToQuery(docQuery, documentTypes);

                documents = docQuery.Execute(long.MaxValue);
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw;
            }

            return documents;
        }

        /// <summary>
        /// Obtiene una lista de Documento de OnBase que cumpla con los criterios indicados.
        /// </summary>
        /// <param name="documentTypes">Tipo de documento a buscar </param>
        /// <param name="filteringKeywords"></param>
        /// <param name="ignoringKeywords"></param>
        /// <returns></returns>
        public DocumentList GetOnBaseDocumentGivenDocumentTypesAndKeywords(List<DocumentType> documentTypes,
                                                                           List<Keyword> filteringKeywords, List<Keyword> ignoringKeywords)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

            DocumentList documents = null;
            DocumentQuery docQuery = _app.Core.CreateDocumentQuery();
            docQuery.RetrievalOptions = DocumentRetrievalOptions.None;

            try
            {
                DocumentQueryUtils.AddKeywordTypeListToQuery(docQuery, filteringKeywords);

                DocumentQueryUtils.AddKeywordValuesToIgnore(docQuery, ignoringKeywords);

                DocumentQueryUtils.AddDocumentTypeListToQuery(docQuery, documentTypes);

                documents = docQuery.Execute(long.MaxValue);
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw;
            }

            return documents;
        }
        /// <summary>
        /// Obtiene un documento de onbase dado su documentHandler
        /// </summary>
        /// <param name="documentHandler">Identificador de documento de Onbase</param>
        /// <returns>Lista de documentos de onbase</returns>
        public Document GetOnbaseDocumentByDocHandler(long documentHandler)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
            Document documentResult = null;

            try
            {
                documentResult = _app.Core.GetDocumentByID(documentHandler);

                return documentResult;
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);

                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);

                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);

                throw;
            }
        }

        /// <summary>
        /// Obtiene una lista de documentos de onbase sus documentHandlers
        /// </summary>
        /// <param name="documentIdList">Lista de DocumentHandlers</param>
        /// <returns>Lista de documentos de onbase</returns>
        public List<Document> GetDocumentByDocumentHandlerList(List<string> documentIdList)
        {
            List<Document> documents = new List<Document>();

            foreach (string documentId in documentIdList)
            {
                long id = Convert.ToInt64(documentId);
                Document doc = GetOnbaseDocumentByDocHandler(id);

                if (doc != null)
                {
                    documents.Add(doc);
                }

            }

            return documents;
        }

        /// <summary>
        /// Actualiza un keyword de un documento dado
        /// </summary>
        /// <param name="document">Documento indicado para actualizar keyword</param>
        /// <param name="newKeywordValue">Nuevo valor del keyword a cambiar</param>
        public void UpdateKeyword(Document document, Keyword newKeywordValue)
        {
            try
            {
                _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;
                _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

                var keywords = GetDocumentKeywordList(document);
                var oldKeyVal = keywords.Find(x => x.KeywordType.Name == newKeywordValue.KeywordType.Name);

                string oldKeywordValue = "0";

                if (oldKeyVal != null)
                {
                    if (!oldKeyVal.IsBlank)
                    {
                        oldKeywordValue = KeywordUtils.GetDefaultKeywordValue(oldKeyVal.KeywordType);
                    }
                }

                KeywordType keywordType = _app.Core.KeywordTypes.Find(newKeywordValue.KeywordType.Name);

                Keyword oldkeyword = KeywordUtils.ConvertToOnbaseKeyword(keywordType, oldKeywordValue);
                Keyword newkeyword = KeywordUtils.ConvertToOnbaseKeyword(keywordType, newKeywordValue.Value.ToString());

                using (DocumentLock documentLock = document.LockDocument())
                {

                    KeywordModifier keyModifier = document.CreateKeywordModifier();
                    keyModifier.UpdateKeyword(oldkeyword, newkeyword);

                    keyModifier.ApplyChanges();
                    _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, string.Format("Llave {0} actualizada", keywordType.Name));
                    documentLock.Release();
                }

            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documento"></param>
        /// <returns></returns>
        public List<Keyword> GetDocumentKeywordList(Document documento)
        {
            var keyRecord = documento.KeywordRecords;
            List<Keyword> keywords = null;

            keywords = new List<Keyword>();

            foreach (var item in keyRecord)
            {
                var listKeyword = item.Keywords;

                foreach (var kitem in listKeyword)
                {
                    var keyword = kitem;
                    keywords.Add(keyword);
                }
            }

            return keywords;
        }

        /// <summary>
        /// Actualiza un lista de keyword para un documento dado
        /// </summary>
        /// <param name="documentToUpdate"> Documento a actualizar</param>
        /// <param name="keywordList"> Lista de keywords a actualizar</param>
        public void UpdateKeywordList(Document documentToUpdate, List<Keyword> keywordList)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

            try
            {
                KeywordModifier keyModifier = documentToUpdate.CreateKeywordModifier();

                using (DocumentLock docLock = documentToUpdate.LockDocument())
                {

                    // Perform steps to modify keywords here
                    foreach (var newKeywordValue in keywordList)
                    {
                        // retrieve the keyword record that contains the keyword to be modified
                        KeywordRecord keyRecord = documentToUpdate.KeywordRecords.Find(newKeywordValue.KeywordType);

                        var defaultValue = KeywordUtils.GetDefaultKeywordValue(newKeywordValue.KeywordType);

                        var oldKeyword = keyRecord == null ? KeywordUtils.ConvertToOnbaseKeyword(newKeywordValue.KeywordType, defaultValue) : keyRecord.Keywords.FindAll(newKeywordValue.KeywordType).FirstOrDefault();

                        keyModifier.UpdateKeyword(oldKeyword, newKeywordValue);
                    }

                    keyModifier.ApplyChanges();

                    docLock.Release();
                }
            }

            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
        }

        public void Delete(Document document)
        {
            try
            {
                _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

                var storage = _app.Core.Storage;

                storage.PurgeDocument(document);

            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw;
            }
        }

        /// <summary>
        /// Obtiene el componente del binario del documento dado
        /// </summary>
        /// <param name="document">Documento cuya binario sera extraido</param>
        /// <returns>Informacion del binario del documento dado</returns>
        public PageData GetPageDataFromDocument(Document document)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

            Rendition rendition = document.DefaultRenditionOfLatestRevision;

            PageData binaryData;

            try
            {
                DefaultDataProvider Provider = _app.Core.Retrieval.Default;
                var pageRangeSet = Provider.CreatePageRangeSet("1-1000");

                Provider.GetPages(rendition, pageRangeSet);

                using (PageData pageData = Provider.GetDocument(rendition))
                {
                    binaryData = pageData;

                }

                return binaryData;
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex.ToString());
                throw;
            }

        }

        /// <summary>
        /// Guarda un documento en Onbase, dado su tipo, keywords y binario
        /// </summary>
        /// <param name="type">Tipo de documento de Onbase</param>
        /// <param name="keywords">Listado de Keywords</param>
        /// <param name="base64ImageFormat">Representación Base64 del binario del documento o imagen a guardar</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewDocument(DocumentType type, List<Keyword> keywords, string base64ImageFormat)
        {
            long newDoc;
            try
            {
                Storage storage = _app.Core.Storage;
                string fileTypeName = "Image File Format";
                FileType filetype = _app.Core.FileTypes.Find(fileTypeName);

                if (filetype == null)
                {
                    throw new Exception(string.Format("File Type [{0}] was not found.", fileTypeName));
                }

                StoreNewDocumentProperties props = storage.CreateStoreNewDocumentProperties(type, filetype);

                foreach (var item in keywords)
                {
                    props.AddKeyword(item);
                }

                MemoryStream p = ConvertBase64StringToMemoryStream(base64ImageFormat);

                PageData pageData = _app.Core.Storage.CreatePageData(p, ".tif");
                Document newDocument = _app.Core.Storage.StoreNewDocument(pageData, props);

                props.DocumentDate = DateTime.Now;
                newDoc = newDocument.ID;
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw new Exception(ex.Message, ex);
            }
            return newDoc;
        }

        /// <summary>
        /// Guarda un UnityForm en Onbase, dado su plantilla y sus keywords.
        /// </summary>
        /// <param name="keywords">Listado de Keywords</param>
        /// <param name="UnityForm">Template del UnityForm</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewUnityForm(List<Keyword> keywords, FormTemplate UnityForm)
        {
            long newDoc;
            try
            {
                StoreNewUnityFormProperties unityFormProps = _app.Core.Storage.CreateStoreNewUnityFormProperties(UnityForm);

                foreach (var item in keywords)
                {
                    unityFormProps.AddKeyword(item);
                }

                Document newDocument = _app.Core.Storage.StoreNewUnityForm(unityFormProps);

                unityFormProps.DocumentDate = DateTime.Now;

                newDoc = newDocument.ID;
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw;
            }
            return newDoc;
        }

        public long SaveNewUnityForm(List<Keyword> keywordList, FormTemplate unityFormTemplate, List<EditableKeywordRecord> onbaseKeyRecords)
        {
            long newDoc;
            try
            {
                Storage storage = _app.Core.Storage;


                StoreNewUnityFormProperties unityFormProps = _app.Core.Storage.CreateStoreNewUnityFormProperties(unityFormTemplate);

                foreach (var key in keywordList)
                {
                    unityFormProps.AddKeyword(key);
                }

                foreach (var keyRecord in onbaseKeyRecords)
                {
                    unityFormProps.AddKeywordRecord(keyRecord);
                }

                Document newDocument = _app.Core.Storage.StoreNewUnityForm(unityFormProps);

                unityFormProps.DocumentDate = DateTime.Now;

                newDoc = newDocument.ID;
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw;
            }
            return newDoc;
        }

        public PageData ConvertToImagePageData(string base64ImageFormat)
        {
            MemoryStream p = ConvertBase64StringToMemoryStream(base64ImageFormat);

            PageData pageData = _app.Core.Storage.CreatePageData(p, ".tif");

            return pageData;
        }



        /// <summary>
        /// Guarda un documento en Onbase, dado su tipo, keywords y binario
        /// </summary>
        /// <param name="type">Tipo de documento de Onbase</param>
        /// <param name="keywords">Listado de Keywords</param>
        /// <param name="base64ImageFormat">Representación Base64 del binario del documento o imagen a guardar</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewDocumentWithPages(DocumentType type, List<PageData> pages, List<Keyword> keywords)
        {
            long newDoc;
            try
            {
                Storage storage = _app.Core.Storage;
                string fileTypeName = "Image File Format";
                FileType filetype = _app.Core.FileTypes.Find(fileTypeName);

                if (filetype == null)
                {
                    throw new ArgumentException(string.Format("File Type [{0}] was not found.", fileTypeName));
                }

                StoreNewDocumentProperties props = storage.CreateStoreNewDocumentProperties(type, filetype);

                foreach (var item in keywords)
                {
                    props.AddKeyword(item);
                }

                Document newDocument = _app.Core.Storage.StoreNewDocument(pages, props);

                props.DocumentDate = DateTime.Now;
                newDoc = newDocument.ID;
            }
            catch (SessionNotFoundException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiUserException(ex.Message, ex);
            }
            catch (UnityAPIException ex)
            {
                _app.Diagnostics.Write(ex);
                throw new UnityApiGeneralException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                _app.Diagnostics.Write(ex);
                throw;
            }
            return newDoc;
        }

        private static MemoryStream ConvertBase64StringToMemoryStream(string base64ImageFormat)
        {

            byte[] arrayByte = Convert.FromBase64String(base64ImageFormat);
            MemoryStream p = new MemoryStream(arrayByte);
            return p;
        }

        /// <summary>
        /// Devuelve el binario de documentHandler dado
        /// </summary>
        /// <param name="documentHandler">identificador del documento de onbase</param>
        /// <returns></returns>
        public byte[] GetImageFromDocument(float documentHandler)
        {
            _app.Diagnostics.Level = Diagnostics.DiagnosticsLevel.Verbose;

            Int64 docHandler = Convert.ToInt64(documentHandler);
            Document doc = _app.Core.GetDocumentByID(docHandler);
            Rendition rendition = doc.DefaultRenditionOfLatestRevision;
            byte[] result = null;

            ImageDataProvider imageProvider = _app.Core.Retrieval.Image;

            if (doc == null)
            {
                //throw new DocumentoExceptions("The  document doesnt exist", Response.FailureDocument);
            }
            FileType fileType = doc.DefaultFileType;
            if (fileType.Name.Equals("Image file format"))
            {
                //throw new DocumentoExceptions("This document file type is not an image", Response.FormatError);
            }
            using (PageData pageData = imageProvider.GetDocument(rendition))
            {
                result = ReadToEnd(pageData.Stream);
                DefaultDataProvider prov = _app.Core.Retrieval.Default;
                PageData pageData1 = prov.GetDocument(rendition);


            }

            return result;
        }

        /// <summary>
        /// Devuelve el binario de un documento dado en formato PDF.
        /// </summary>
        /// <param name="documentHandler"> identificador del documento de onbase</param>
        /// <returns></returns>
        public byte[] GetOnBaseDocumentPDF(float documentHandler)
        {
            Int64 docHandler = Convert.ToInt64(documentHandler);
            Document doc = _app.Core.GetDocumentByID(docHandler);
            Rendition rendition = doc.DefaultRenditionOfLatestRevision;
            byte[] result = null;

            PDFDataProvider imageProvider = _app.Core.Retrieval.PDF;
            if (doc == null)
            {
                //throw new DocumentoExceptions("The  document doesnt exist", Response.FailureDocument);
            }
            FileType fileType = doc.DefaultFileType;

            if (fileType.Name.Equals("Image file format"))
            {
                // throw new DocumentoExceptions("This document file type is not an PDF", Response.FormatError);
            }
            using (PageData pageData = imageProvider.GetDocument(rendition))
            {
                result = ReadToEnd(pageData.Stream);
            }


            return result;
        }

        private byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }
            try
            {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
