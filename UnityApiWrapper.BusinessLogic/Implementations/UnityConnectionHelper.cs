﻿using Hyland.Unity;
using System;
using UnityApiWrapper.BusinessLogic.Contracts;
using UnityApiWrapper.Models.Exceptions;

namespace UnityApiWrapper.BusinessLogic.Implementations
{
    /// <summary>
    /// Responsable de manejar hacia Onbase utilizando UnityApi
    /// </summary>
    public sealed class UnityConnectionHelper : IUnityConnector
    {
        private Application _app = null;

        /// <summary>
        /// Constructor que recibe el objeto de conexion establecida de onbase
        /// </summary>
        /// <param name="app"></param>
        public UnityConnectionHelper(Application app)
        {
            _app = app;
        }

        public UnityConnectionHelper()
        {

        }

        /// <summary>
        /// Realiza la conexion hacia onbase en modo conectado.
        /// </summary>
        /// <param name="credentials">Credenciales de Onbase para establecer conexión</param>
        public void ConnectInConnectedMode(IOnbaseCredentials credentials)
        {
            try
            {
                OnBaseAuthenticationProperties properties = Application.CreateOnBaseAuthenticationProperties(credentials.GetAppServerUrl(),
                                                                                                                          credentials.GetOnbaseUserName(),
                                                                                                                          credentials.GetOnbaseUserPassword(),
                                                                                                                          credentials.GetOnbaseDatasource());
                
                properties.LicenseType = LicenseType.QueryMetering;
                
                _app = Application.Connect(properties);
                

            }
            catch (InvalidLoginException ex)
            {
                throw new UnityApiUserException("The credentials entered are invalid. " + ex.ToString(), ex);
            }
            catch (UserAccountLockedException ex)
            {
                throw new UnityApiUserException("The user account is locked. " + ex.ToString(), ex);
            }
            catch (AuthenticationFailedException ex)
            {
                throw new UnityApiUserException("Authentication failed. " + ex.ToString(), ex);
            }
            catch (MaxConcurrentLicensesException ex)
            {
                throw new UnityApiUserException("All licenses are currently in use, please try again later. " + ex.ToString(), ex);
            }
            catch (NamedLicenseNotAvailableException ex)
            {
                throw new UnityApiUserException("Your license is not availble, please insure you are logged out of other OnBase clients. " + ex.ToString(), ex);
            }
            catch (SystemLockedOutException ex)
            {
                throw new UnityApiUserException("The system is currently locked, please try back later. " + ex.ToString(), ex);
            }
            catch (UnityAPIException ex)
            {
                throw new UnityApiGeneralException("There was an unhandled exception with the Unity API. " + ex.ToString(), ex);
            }
            catch (Exception ex)
            {
                throw new UnityApiGeneralException("There was an unhandled exception. " + ex.ToString(), ex);
            }
        }

        /// <summary>
        /// Realiza la conexion hacia onbase utilizando licencias nombradas
        /// </summary>
        /// <param name="credentials">Credenciales de Onbase para establecer conexión</param>
        public void ConnectUsingNamedLicense(IOnbaseCredentials credentials)
        {
            try
            {
                OnBaseAuthenticationProperties properties = Application.CreateOnBaseAuthenticationProperties(credentials.GetAppServerUrl(),
                                                                                                                          credentials.GetOnbaseUserName(),
                                                                                                                          credentials.GetOnbaseUserPassword(),
                                                                                                                          credentials.GetOnbaseDatasource());
                // properties.LicenseType = LicenseType.QueryMetering;
                _app = Application.Connect(properties);
                _app.Timeout = 300000;

            }
            catch (InvalidLoginException ex)
            {
                throw new UnityApiUserException("The credentials entered are invalid. " + ex.ToString(), ex);
            }
            catch (AuthenticationFailedException ex)
            {
                throw new UnityApiUserException("Authentication failed. " + ex.ToString(), ex);
            }
            catch (MaxConcurrentLicensesException ex)
            {
                throw new UnityApiUserException("All licenses are currently in use, please try again later. " + ex.ToString(), ex);
            }
            catch (NamedLicenseNotAvailableException ex)
            {
                throw new UnityApiUserException("Your license is not availble, please insure you are logged out of other OnBase clients. " + ex.ToString(), ex);
            }
            catch (SystemLockedOutException ex)
            {
                throw new UnityApiUserException("The system is currently locked, please try back later. " + ex.ToString(), ex);
            }
            catch (UnityAPIException ex)
            {
                throw new UnityApiGeneralException("There was an unhandled exception with the Unity API. " + ex.ToString(), ex);
            }
            catch (Exception ex)
            {
                throw new UnityApiGeneralException("There was an unhandled exception. " + ex.ToString(), ex);
            }
        }


        /// <summary>
        /// Permite realizar la desconexión de la sesion iniciada en Onbase
        /// </summary>
        /// <param name="app">Objeto de conexion de UnityApi dado</param>
        public void Disconnect(Application app)
        {
            if (app != null)
            {
                app.Disconnect();
            }
        }

        /// <summary>
        /// Permite obtener la conexión actual habilitada.
        /// </summary>
        /// <returns></returns>
        public Application GetCurrentConnection()
        {
            return _app;
        }

        /// <summary>
        /// Permite realizar la desconexión de la sesion iniciada en Onbase
        /// </summary>
        public void Disconnect()
        {
            if (_app != null)
            {
                _app.Disconnect();

            }
        }
    }
}
