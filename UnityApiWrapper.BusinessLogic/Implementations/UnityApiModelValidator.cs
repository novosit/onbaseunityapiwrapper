﻿using Hyland.Unity;
using System;
using System.Collections.Generic;
using UnityApiWrapper.BusinessLogic.Contracts;
using UnityApiWrapper.Models.Exceptions;
using UnityApiWrapper.OnbaseDataAccess;

namespace UnityApiWrapper.BusinessLogic.Implementations
{
    /// <summary>
    /// Provee la funcionalidad de validación de modelos de Onbase
    /// </summary>
    public sealed class UnityApiModelValidator : IUnityApiModelValidator
    {
        private IUnityConnector _connector;

        public UnityApiModelValidator(IUnityConnector connector)
        {
            _connector = connector;
        }

        /// <summary>
        /// Verifica si un listado de keyword es válido en Onbse
        /// </summary>
        /// <param name="searchKeywords">keywords a ser validados</param>
        public void ValidateKeywordList(List<Models.KeywordModel> searchKeywords)
        {
            foreach (var keyword in searchKeywords)
            {
                ValidateKeywordType(keyword);
            }
        }

        /// <summary>
        /// Verifica si un keyword indicado existen en Onbase
        /// </summary>
        /// <param name="keyword">keyword a validar</param>
        public void ValidateKeywordType(Models.KeywordModel keyword)
        {
            KeywordType keywordType = KeywordUtils.FindKeywordTypeByName(keyword.TypeName, _connector.GetCurrentConnection());

            if (keywordType == null)
            {
                string message = string.Format("El keyword con el tipo [{0}] no existe. Verifique por favor", keyword.TypeName);

                throw new InvalidKeywordTypeException(message);
            }
        }

        /// <summary>
        /// Verifica si una lista de keywords existen en onbase
        /// </summary>
        /// <param name="keywordTypes"></param>
        public void ValidateKeywordTypeList(IEnumerable<Models.KeywordModel> keywordTypes)
        {
            foreach (var keywordType in keywordTypes)
            {
                ValidateKeywordType(keywordType);
            }
        }

        /// <summary>
        /// Valida si un tipo de documento existe por nombre
        /// </summary>
        /// <param name="documentType">Tipo de documento de onbase</param>
        public void ValidateDocumentType(Models.DocumentTypeModel documentType)
        {
            DocumentType docType = OnbaseDocumentUtils.FindDocumentTypeByName(documentType.Name, _connector.GetCurrentConnection());

            if (docType == null)
            {
                string message = string.Format("El Documento con el tipo [{0}] no existe. Verifique por favor", documentType.Name);
                throw new InvalidDocumentTypeException(message);
            }
        }

        /// <summary>
        /// Verifica si un tipo de documento es valido
        /// </summary>
        /// <param name="documentType">Identificador del tipo de documento</param>
        public void ValidateDocumentTypeById(Models.DocumentTypeModel documentType)
        {
            DocumentType docType = OnbaseDocumentUtils.FindDocumentTypeById(documentType.Id, _connector.GetCurrentConnection());

            if (docType == null)
            {
                string message = string.Format("El Documento con el tipo [{0}] no existe. Verifique por favor", documentType.Id);
                throw new InvalidDocumentTypeException(message);
            }
        }

        /// <summary>
        ///  Verifica si un listado de keyword es válido en Onbse
        /// </summary>
        /// <param name="documentTypes"></param>
        public void ValidateDocumentTypeList(List<Models.DocumentTypeModel> documentTypes)
        {
            foreach (var documentType in documentTypes)
            {
                ValidateDocumentTypeById(documentType);
            }
        }

        /// <summary>
        /// Verifica la existencia de un documento en Onbase
        /// </summary>
        /// <param name="documentHandler">Identificador de documento de onbase</param>
        public void ValidateDocumentExistence(string documentHandler)
        {
            long docId = Convert.ToInt64(documentHandler);

            UnityApiDataAccess dataAccess = new UnityApiDataAccess(_connector.GetCurrentConnection());
            Document document = dataAccess.GetOnbaseDocumentByDocHandler(docId);

            if (document == null)
            {
                string message = string.Format("El documento con el documentHandler [{0}] no existe. Verifique por favor", documentHandler);
                throw new ArgumentException(message);
            }
        }
    }
}
