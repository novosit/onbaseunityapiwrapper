﻿using System.Configuration;
using UnityApiWrapper.BusinessLogic.Contracts;

namespace UnityApiWrapper.BusinessLogic.Implementations
{
    /// <summary>
    /// Proporciona la funcionaliad para acceder a los parametros de configuracion de Onbase.
    /// </summary>
    public sealed class OnbaseConfigFile : IOnbaseCredentials
    {
        /// <summary>
        /// Devuelve el usuario configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseUserName()
        {
            return ConfigurationManager.AppSettings["OnbaseUserName"];
        }

        /// <summary>
        /// Devuelve la contraseña configurada para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseUserPassword()
        {
            return ConfigurationManager.AppSettings["OnbaseUserPassword"];
        }

        /// <summary>
        /// Devuelve la url del appserver configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>v
        public string GetAppServerUrl()
        {
            return ConfigurationManager.AppSettings["OnbaseAppServerUrl"];
        }

        /// <summary>
        /// Devuelve el nombre del Datasource(ODBC) configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseDatasource()
        {
            return ConfigurationManager.AppSettings["OnbaseDatasource"];
        }
    }
}
