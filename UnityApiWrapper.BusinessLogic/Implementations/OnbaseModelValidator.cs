﻿using System;
using System.Collections.Generic;
using UnityApiWrapper.BusinessLogic.Contracts;

namespace UnityApiWrapper.BusinessLogic.Implementations
{
    /// <summary>
    ///Es responsable de manejar las validaciones de documentHandlers de Onbase
    /// </summary>
    public sealed class ModelValidator : IModelValidator
    {
        /// <summary>
        /// Verifica que la lista de documentHandlers sean formato Entero.
        /// </summary>
        /// <param name="documentHandlers">Representa un lista de documentHandlers de documentos de onbase.</param>
        public void ValidateDocumentHandlerList(List<string> documentHandlers)
        {
            if (documentHandlers == null || documentHandlers.Count == 0)
            {
                string message = "La lista de document Handlers no puede estar vacia. Verifique por favor!";
                throw new ArgumentException(message, nameof(documentHandlers));
            }

            foreach (string documentHandler in documentHandlers)
            {
                ValidateDocumentHandler(documentHandler);
            }
        }

        /// <summary>
        /// Verifica si un documentHandler esta en formato entero
        /// </summary>
        /// <param name="documentHandler">Representa un identificador de documento de onbase</param>
        public void ValidateDocumentHandler(string documentHandler)
        {
            long conversionResult = 0;
            bool conversionFailed = !long.TryParse(documentHandler, out conversionResult);

            if (conversionFailed)
            {
                string message = string.Format("El documentHandler [{0}] no es un número. Verifique por favor", documentHandler);
                throw new ArgumentException(message, nameof(documentHandler));
            }
        }
    }
}
