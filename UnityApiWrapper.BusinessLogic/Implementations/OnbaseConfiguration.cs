﻿using UnityApiWrapper.BusinessLogic.Contracts;

namespace UnityApiWrapper.BusinessLogic.Implementations
{
    /// <summary>
    ///  Provee los parametros de configuracion de Onbase para conectarse a UnityApi
    /// </summary>
    public sealed class OnbaseConfigurationTest : IOnbaseCredentials
    {
        /// <summary>
        /// Devuelve el usuario configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseUserName()
        {
            return "MANAGER";
        }

        /// <summary>
        /// Devuelve la contraseña configurada para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseUserPassword()
        {
            return "PASSWORD";
        }

        /// <summary>
        /// Devuelve la url del appserver configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetAppServerUrl()
        {
            return "http://localhost/AppServer/Service.asmx";
        }

        /// <summary>
        /// Devuelve el nombre del Datasource(ODBC) configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        public string GetOnbaseDatasource()
        {
            return "OnBase";
        }
    }
}
