﻿using System.Collections.Generic;

namespace UnityApiWrapper.BusinessLogic.Contracts
{
    /// <summary>
    /// Representa la responsabilidad de manejar validaciones de documentHandlers de Onbase
    /// </summary>
    public interface IModelValidator
    {
        /// <summary>
        /// Verifica que la lista de documentHandlers sean formato Entero.
        /// </summary>
        /// <param name="documentHandlers">Representa un lista de documentHandlers de documentos de onbase.</param>
        void ValidateDocumentHandlerList(List<string> documentHandlers);

        /// <summary>
        /// Verifica si un documentHandler esta en formato entero
        /// </summary>
        /// <param name="documentHandler">Representa un identificador de documento de onbase</param>
        void ValidateDocumentHandler(string documentHandler);

    }
}
