﻿
namespace UnityApiWrapper.BusinessLogic.Contracts
{
    /// <summary>
    /// Representa la responsabilidad de establecer conexion con UnityApi
    /// </summary>
    public interface IUnityConnector
    {
        /// <summary>
        /// Realiza la conexion hacia onbase en modo conectado.
        /// </summary>
        /// <param name="credentials">Credenciales de Onbase para establecer conexión</param>
        void ConnectInConnectedMode(IOnbaseCredentials credentials);

        /// <summary>
        /// Realiza la conexion hacia onbase utilizando licencias nombradas
        /// </summary>
        /// <param name="credentials">Credenciales de Onbase para establecer conexión</param>
        void ConnectUsingNamedLicense(IOnbaseCredentials credentials);

        /// <summary>
        /// Permite obtener la conexión actual habilitada.
        /// </summary>
        /// <returns></returns>
        Hyland.Unity.Application GetCurrentConnection();

        /// <summary>
        /// Permite realizar la desconexión de la sesion iniciada en Onbase
        /// </summary>
        void Disconnect();
    }
}
