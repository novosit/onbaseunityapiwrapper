﻿
namespace UnityApiWrapper.BusinessLogic.Contracts
{
    /// <summary>
    /// Representa la responsabilidad de proveer los parametros de configuracion de Onbase para trabajar con UnityApi.
    /// </summary>
    public interface IOnbaseCredentials
    {
        /// <summary>
        /// Devuelve el usuario configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        string GetOnbaseUserName();

        /// <summary>
        /// Devuelve la contraseña configurada para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        string GetOnbaseUserPassword();

        /// <summary>
        /// Devuelve la url del appserver configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        string GetAppServerUrl();

        /// <summary>
        /// Devuelve el nombre del Datasource(ODBC) configurado para conectarse a Onbase a traves de UnityApi
        /// </summary>
        /// <returns></returns>
        string GetOnbaseDatasource();
    }
}
