﻿using System.Collections.Generic;

namespace UnityApiWrapper.BusinessLogic.Contracts
{
    /// <summary>
    /// Representa la responsabilidad de la validación de modelos de Onbase.
    /// </summary>
    public interface IUnityApiModelValidator
    {
        /// <summary>
        /// Verifica si una lista de tipos de documentos es valida.
        /// </summary>
        /// <param name="documentTypes"></param>
        void ValidateDocumentTypeList(List<Models.DocumentTypeModel> documentTypes);

        /// <summary>
        /// Verifica la existencia de un documento en Onbase
        /// </summary>
        /// <param name="documentHandler">Identificador de documento de onbase</param>
        void ValidateDocumentExistence(string documentHandler);

        /// <summary>
        /// Valida si un tipo de documento existe por nombre
        /// </summary>
        /// <param name="documentType">Tipo de documento de onbase</param>
        void ValidateDocumentType(Models.DocumentTypeModel documentType);

        /// <summary>
        /// Verifica si un tipo de documento es valido
        /// </summary>
        /// <param name="documentType">Identificador del tipo de documento</param>
        void ValidateDocumentTypeById(Models.DocumentTypeModel documentType);

        /// <summary>
        /// Verifica si un keyword indicado es válido en Onbase
        /// </summary>
        /// <param name="keyword"></param>
        void ValidateKeywordType(Models.KeywordModel keyword);

        /// <summary>
        /// Verifica si un listado de keyword es válido en Onbse
        /// </summary>
        /// <param name="searchKeywords"></param>
        void ValidateKeywordList(List<Models.KeywordModel> searchKeywords);
    }
}
