﻿using Hyland.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityApiWrapper.Models;
using UnityApiWrapper.Models.Contracts;
using UnityApiWrapper.Models.Implementations;
using UnityApiWrapper.Models.Models;
using UnityApiWrapper.OnbaseDataAccess;

namespace UnityApiWrapper.BusinessLogic.Mappers
{
    /// <summary>
    /// Provee la funcionalidad de convertir documentos de Onbase en documentos de validacion
    /// </summary>
    public  static class ValidationDocumentMapper
    {
        /// <summary>
        /// Devuelve una lista de tipos de documentos no encontrados
        /// </summary>
        /// <param name="documentTypesFilters">Tipos de documentos a buscar</param>
        /// <param name="onbaseDocs"> Lista de documentos de onbase</param>
        /// <returns>Lista de informacion de documentos no encontrados </returns>
        private static List<ValidationDocument> GetNotFoundDocumentTypes(List<DocumentTypeModel> documentTypesFilters, DocumentList onbaseDocs)
        {
            List<ValidationDocument> resultDocuments = new List<ValidationDocument>();

            foreach (var documentType in documentTypesFilters)
            {
                bool notFound = !onbaseDocs.Any(x => x.DocumentType.Name.Equals(documentType.Name));

                if (notFound)
                {
                    resultDocuments.Add(ConvertToNotFoundDocument(documentType));
                }
            }

            return resultDocuments;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentType"></param>
        /// <returns></returns>
        private static ValidationDocument ConvertToNotFoundDocument(DocumentTypeModel documentType)
        {
            return new ValidationDocument()
            {
                DocumentTypeId = documentType.Id.ToString(),
                DocumentHandler = "0",
                Existe = false,
                Vigencia = string.Empty,
                FechaExpiracion = string.Empty
            };
        }

        private static List<ValidationDocument> ConvertToNotFoundValidationDocuments(List<DocumentTypeModel> DocumentTypesFilters)
        {
            var notFoundDocuments = DocumentTypesFilters.Select(x => ConvertToNotFoundDocument(x)).ToList();

            return notFoundDocuments;
        }

        private static List<ValidationDocument> ConvertModelToValidationDocumentList(DocumentList onbaseDocs, List<DocumentTypeModel> documentTypesFilters)
        {
            List<ValidationDocument> result = new List<ValidationDocument>();

            if (!onbaseDocs.Any())
            {
                return result;
            }

            foreach (var doc in onbaseDocs)
            {
                ValidationDocument document = ConvertModelToValidationDocument(doc);
                result.Add(document);
            }

            result.AddRange(ValidationDocumentMapper.GetNotFoundDocumentTypes(documentTypesFilters, onbaseDocs));

            return result;
        }


        /// <summary>
        /// Convierte un documento de Onbase en un documento de validación
        /// </summary>
        /// <param name="doc">Documento de Onbase</param>
        /// <returns></returns>
        private static ValidationDocument ConvertModelToValidationDocument(Document doc)
        {
            List<Keyword> keywords = KeywordUtils.GetDocumentKeywordList(doc);

            ValidationDocument document = new ValidationDocument
            {
                DocumentHandler = doc.ID.ToString(),
                DocumentTypeId = doc.DocumentType.ID.ToString(),
                Existe = true
            };

            IKeywordsConfiguration keywordConf = new KeywordsConfiguration();
            var keywordVigencia = keywords.FirstOrDefault(x => x.KeywordType.Name.Equals(keywordConf.GetKeywordVigenciaName()));
            var keywordFechaExpiracion = keywords.FirstOrDefault(x => x.KeywordType.Name.Equals(keywordConf.GetKeywordFechaExpiracion()));

            document.Vigencia = keywordVigencia == null ? string.Empty : keywordVigencia.Value.ToString();
            document.FechaExpiracion = keywordFechaExpiracion == null ? string.Empty : keywordFechaExpiracion.Value.ToString();

            return document;
        }

        /// <summary>
        /// Convierte un DocumentList de Onbase a una lista de modelos OnbaseDocument
        /// </summary>
        /// <param name="documents">Lista de documentos de onbase</param>
        /// <param name="onbaseAgent">Componente encargada de manejar logica de negocio</param>
        /// <returns></returns>
        public static List<OnbaseDocument> ConvertToOnbaseDocumentList(DocumentList documents, OnbaseBL onbaseAgent)
        {
            List<OnbaseDocument> result = new List<OnbaseDocument>();

            foreach (var document in documents)
            {
                OnbaseDocument doc = ConvertToOnbaseDocument(document, onbaseAgent);
                result.Add(doc);
            }

            return result;
        }

        /// <summary>
        /// Convierte un Lista de documentos de Onbase a una lista de modelos OnbaseDocument
        /// </summary>
        /// <param name="documents">Lista de documentos de onbase</param>
        /// <param name="onbaseAgent">Componente encargada de manejar logica de negocio</param>
        /// <returns></returns>
        public static List<OnbaseDocument> ConvertToOnbaseDocumentList(List<Document> documents, OnbaseBL onbaseAgent)
        {
            List<OnbaseDocument> result = new List<OnbaseDocument>();

            foreach (var document in documents)
            {
                OnbaseDocument doc = ConvertToOnbaseDocument(document, onbaseAgent);
                result.Add(doc);
            }

            return result;
        }
        private static OnbaseDocument ConvertToOnbaseDocument(Document document, OnbaseBL onbaseAgent)
        {
            List<Keyword> keywords = KeywordUtils.GetDocumentKeywordList(document);

            OnbaseDocument result = new OnbaseDocument
            {
                DocumentHandler = document.ID,
                DocumentTypeId = document.DocumentType.ID,
                DocumentTypeName = document.DocumentType.Name,
                Image = onbaseAgent.GetBinaryFromDocument(document),
                Keywords = keywords.Select(x => new KeywordModel() { TypeName = x.KeywordType.Name, Value = x.ToString() }).ToList()
            };

            return result;
        }
    }
}
