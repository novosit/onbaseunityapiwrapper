﻿using Hyland.Unity;
using Hyland.Unity.UnityForm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityApiWrapper.BusinessLogic.Contracts;
using UnityApiWrapper.BusinessLogic.Implementations;
using UnityApiWrapper.Models;
using UnityApiWrapper.Models.Exceptions;
using UnityApiWrapper.Models.Models;
using UnityApiWrapper.OnbaseDataAccess;

namespace UnityApiWrapper.BusinessLogic
{
    /// <summary>
    /// Es responsable de las operaciones de acceso a Datos hacia Onbase utilizando UnityAPI
    /// </summary>
    public sealed class OnbaseBL
    {
        private readonly IUnityConnector _connector;
        private readonly IOnbaseCredentials _onbaseCredentials;
        private readonly IModelValidator _modelValidator;
        private readonly IUnityApiModelValidator _UnityModelValidator;
        private bool _connectionIsEstablished;

        /// <summary>
        /// Constructor que inicializar todas las dependencias con valores por defecto
        /// </summary>
        public OnbaseBL()
        {
            _onbaseCredentials = new OnbaseConfigFile();
            _connector = new UnityConnectionHelper();
            _modelValidator = new ModelValidator();
            _UnityModelValidator = new UnityApiModelValidator(_connector);
        }
        /// <summary>
        /// Constructor que recibe componente que provee las credenciales de onbase
        /// </summary>
        /// <param name="onbaseCredentials"></param>
        public OnbaseBL(IOnbaseCredentials onbaseCredentials)
        {
            _connector = new UnityConnectionHelper();
            _onbaseCredentials = onbaseCredentials;
            _modelValidator = new ModelValidator();
            _UnityModelValidator = new UnityApiModelValidator(_connector);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connector"></param>
        /// <param name="onbaseCredentials"></param>
        public OnbaseBL(IUnityConnector connector, IOnbaseCredentials onbaseCredentials)
        {
            _connector = connector;
            _onbaseCredentials = onbaseCredentials;
            _modelValidator = new ModelValidator();
            _UnityModelValidator = new UnityApiModelValidator(_connector);
        }

        /// <summary>
        /// Inicia la conexion con Onbase A traves de UnityAPI usando Query Hour
        /// </summary>
        public void BeginConnectionWithQueryHour()
        {
            //TODO: Cambiar
            _connector.ConnectInConnectedMode(_onbaseCredentials);

            _connectionIsEstablished = true;
        }

        /// <summary>
        /// Inicia la conexion con Onbase A traves de UnityAPI usando Named License
        /// </summary>
        public void BeginConnectionWithNamedLicense()
        {
            //TODO: Cambiar
            _connector.ConnectUsingNamedLicense(_onbaseCredentials);
            _connectionIsEstablished = true;
        }

        /// <summary>
        /// Termina la conexion actual con Onbase.
        /// </summary>
        public void CloseConnection()
        {
            _connector.Disconnect();
            _connectionIsEstablished = false;
        }

        /// <summary>
        /// Obtiene un conjunto de documentos de onbase dado unos criterios de Búsqueda
        /// </summary>
        /// <param name="documentTypes">Lista de tipos de documentos a buscar</param>
        /// <param name="searchKeywords">Lista de keywords a buscar</param>
        /// <returns>Lusta de Documentos</returns>
        public DocumentList GetOnbaseDocuments(List<DocumentTypeModel> documentTypes, List<KeywordModel> searchKeywords)
        {
            _UnityModelValidator.ValidateDocumentTypeList(documentTypes);
            _UnityModelValidator.ValidateKeywordList(searchKeywords);

            UnityApiDataAccess dataAccess = new UnityApiDataAccess(_connector.GetCurrentConnection());

            List<DocumentType> OnbaseDocumentTypes = OnbaseDocumentUtils.ConvertToOnbaseDocumentTypeList(documentTypes, _connector.GetCurrentConnection());

            List<Keyword> onbaseKeywords = KeywordUtils.ConvertToOnbaseKeywordList(searchKeywords, _connector.GetCurrentConnection());

            return dataAccess.GetOnBaseDocumentGivenDocumentTypesAndKeywords(OnbaseDocumentTypes, onbaseKeywords);
        }

        /// <summary>
        /// Obtiene una lista de Documentos dado sus documentHandlers
        /// </summary>
        /// <param name="documentHandlers">Lista de DocumentHandlers</param>
        /// <returns>Lista de Documentos</returns>
        public List<Document> GetDocumentsByDocumentHandler(List<string> documentHandlers)
        {
            _modelValidator.ValidateDocumentHandlerList(documentHandlers);

            UnityApiDataAccess dataAccess = new UnityApiDataAccess(_connector.GetCurrentConnection());
            List<Document> result = dataAccess.GetDocumentByDocumentHandlerList(documentHandlers);

            return result;
        }

        /// <summary>
        /// Obtiene el binario de un documento de onbase
        /// </summary>
        /// <param name="document">Documento de Onbase </param>
        /// <returns>Binario del Documento</returns>
        public DocumentBinary GetBinaryFromDocument(Document document)
        {
            DocumentBinary result = ConvertToDocumentBinary(document);

            return result;
        }

        /// <summary>
        /// Elimina un documento de onbase dado su DocumentHandler
        /// </summary>
        /// <param name="documentHandler">identificador del documento a eliminar</param>
        public void DeleteDocument(long documentHandler)
        {
            UnityApiDataAccess dataAccess = new UnityApiDataAccess(_connector.GetCurrentConnection());
            List<string> docHandlers = new List<string>() { documentHandler.ToString() };

            var document = GetDocumentsByDocumentHandler(docHandlers);

            if (document.Count > 0)
            {
                dataAccess.Delete(document.First());
            }
            else
            {
                //loguear que no se encontro
            }
        }

        private DocumentBinary ConvertToDocumentBinary(Document document)
        {
            PageData pagedata = _connector.GetCurrentConnection().Core.Retrieval.Image.GetDocument(document.DefaultRenditionOfLatestRevision);

            byte[] result = ConvertToByteArray(pagedata.Stream);

            DocumentBinary binaryResult = new DocumentBinary()
            {
                Extension = pagedata.Extension,
                Base64String = Convert.ToBase64String(result, Base64FormattingOptions.None)
            };

            pagedata.Dispose();

            return binaryResult;
        }

        private byte[] ConvertToByteArray(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }
            try
            {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;

                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        /// <summary>
        /// Actualiza un keywords de un documento de Onbase, dado su documentHandler y keyword.
        /// </summary>
        /// <param name="documentHandler">Identificador de documento de Onbase</param>
        /// <param name="keyword">Keyword a realizar el cambio</param>
        /// <returns>Status de la operacion realizado</returns>
        public OperationStatus UpdateDocumentKeyword(string documentHandler, KeywordModel keyword)
        {
            OperationStatus result = new OperationStatus { Status = true };
            Application currentApp = _connector.GetCurrentConnection();

            try
            {
                _UnityModelValidator.ValidateKeywordType(keyword);

                UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(_connector.GetCurrentConnection());
                long docId = Convert.ToInt64(documentHandler);

                Document document = unityApiAgent.GetOnbaseDocumentByDocHandler(docId);
                KeywordType keywordType = KeywordUtils.FindKeywordTypeByName(keyword.TypeName, currentApp);

                Keyword newKeyword = KeywordUtils.ConvertToOnbaseKeyword(keywordType, keyword.Value);
                unityApiAgent.UpdateKeyword(document, newKeyword);
            }
            catch (Exception ex)
            {
                return OperationStatus.CreateFromException(ex.Message, ex);
            }

            return result;
        }

        /// <summary>
        /// Actualiza una lista de keywords de un documento dado
        /// </summary>
        /// <param name="documentHandler">Identificador del documento a actualizar</param>
        /// <param name="keywords">Lista de nuevos valores de keywords</param>
        public void UpdateDocumentKeywordList(string documentHandler, List<KeywordModel> keywords)
        {
            Application currentApp = _connector.GetCurrentConnection();

            _UnityModelValidator.ValidateKeywordList(keywords);

            long docId = Convert.ToInt64(documentHandler);

            UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(currentApp);
            Document document = unityApiAgent.GetOnbaseDocumentByDocHandler(docId);

            if (document == null)
            {
                throw new ArgumentException(string.Format("Documento:{0} no existe, verifique por favor", documentHandler), nameof(documentHandler));
            }

            List<Keyword> keywordsToUpdate = KeywordUtils.ConvertToOnbaseKeywordList(keywords, currentApp);

            unityApiAgent.UpdateKeywordList(document, keywordsToUpdate);
        }

        /// <summary>
        /// Actualiza un keyword de una lista de documentos dado
        /// </summary>
        /// <param name="documentHandlersList">Lista de identificadores de documento de Onbase</param>
        /// <param name="newKeywordValue">Keyword a ser actualizado y su valor</param>
        public void UpdateKeywordToGivenDocuments(DocumentList documentHandlersList, KeywordModel newKeywordValue)
        {
            foreach (Document document in documentHandlersList)
            {
                var updateResult = UpdateDocumentKeyword(document.ID.ToString(), newKeywordValue);

                if (updateResult.Status == false)
                {
                    var Message = string.Format("El documento con el dochandler [{0}], presentó el siguiente error:[{1}] \n \n ",
                        document.ID, updateResult.ExceptionStackTrace);

                    throw new ArgumentException(Message);
                }
            }
        }

        /// <summary>
        /// Permite guardar un documento en Onbase dado su tipo, binario y Keywords
        /// </summary>
        /// <param name="documentType">Tipo de Documento de Onbase</param>
        /// <param name="imageInBase64StringFormat">Binario en formato Base64</param>
        /// <param name="keywordToAdd">Keywords que se desean almacenar</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewDocument(DocumentTypeModel documentType, string imageInBase64StringFormat, List<KeywordModel> keywordToAdd)
        {
            Application currentApp = _connector.GetCurrentConnection();

            ValidateDocument(documentType);

            _UnityModelValidator.ValidateKeywordList(keywordToAdd);

            UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(currentApp);

            DocumentType docType = FindDocumentType(documentType);

            List<Keyword> keywordList = KeywordUtils.ConvertToOnbaseKeywordList(keywordToAdd, currentApp);

            long id = unityApiAgent.SaveNewDocument(docType, keywordList, imageInBase64StringFormat);

            return id;
        }

        /// <summary>
        /// Permite guardar un documento en Onbase dado su tipo, sus paginas y Keywords
        /// </summary>
        /// <param name="documentType">Tipo de Documento de Onbase</param>
        /// <param name="pagesInBase64Format">Paginas del documento en formato base64String </param>
        /// <param name="keywordToAdd">Keywords que se desean almacenar</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewDocumentWithPages(DocumentTypeModel documentType, List<string> pagesInBase64Format, List<KeywordModel> keywordToAdd)
        {
            ValidateDocument(documentType);

            //Validar keywords
            _UnityModelValidator.ValidateKeywordList(keywordToAdd);

            UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(_connector.GetCurrentConnection());
            DocumentType docType;

            docType = FindDocumentType(documentType);

            List<Keyword> keywordList = KeywordUtils.ConvertToOnbaseKeywordList(keywordToAdd, _connector.GetCurrentConnection());

            List<PageData> documentPages = new List<PageData>();

            foreach (var page in pagesInBase64Format)
            {
                documentPages.Add(item: unityApiAgent.ConvertToImagePageData(page));
            }

            long id = unityApiAgent.SaveNewDocumentWithPages(docType, documentPages, keywordList);

            return id;
        }

        /// <summary>
        /// Permite guardar un UnityForm en Onbase dado su plantilla y Keywords
        /// </summary>

        /// <param name="documentType">Tipo de documento UnityForm</param>
        /// <param name="keywordToAdd">Keywords que se desean almacenar</param>
        /// <returns>DocumentHandler del documento creado</returns>
        public long SaveNewUnityForm(DocumentTypeModel documentType, List<KeywordModel> keywordToAdd)
        {
            Application currentApp = _connector.GetCurrentConnection();

            FormTemplate UnityFormTemplate = currentApp.Core.UnityFormTemplates.Find(documentType.Name);

            if (UnityFormTemplate == null)
            {
                throw new InvalidDocumentTypeException(string.Format("La plantilla [{0}] no se pudo encontrar en Onbase", documentType.Name));
            }

            //Validar keywords
            _UnityModelValidator.ValidateKeywordList(keywordToAdd);

            UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(currentApp);

            List<Keyword> keywordList = KeywordUtils.ConvertToOnbaseKeywordList(keywordToAdd, currentApp);
            
            long id = unityApiAgent.SaveNewUnityForm(keywordList, UnityFormTemplate);

            return id;
        }


        public long SaveNewUnityForm(DocumentTypeModel documentType, List<KeywordModel> keywords, List<KeywordRecordModel> keywordRecords)
        {
            Application currentApp = _connector.GetCurrentConnection();

            FormTemplate UnityFormTemplate = currentApp.Core.UnityFormTemplates.Find(documentType.Name);

            if (UnityFormTemplate == null)
            {
                throw new InvalidDocumentTypeException(string.Format("La plantilla [{0}] no se pudo encontrar en Onbase", documentType.Name));
            }

            //Validar keywords
            _UnityModelValidator.ValidateKeywordList(keywords);

            //Validar todos los keywords dentro de los keywordRecord
            foreach (var keywordRecord in keywordRecords)
            {
                _UnityModelValidator.ValidateKeywordList(keywordRecord.Keywords);
            }

            UnityApiDataAccess unityApiAgent = new UnityApiDataAccess(currentApp);

            List<Keyword> keywordList = KeywordUtils.ConvertToOnbaseKeywordList(keywords, currentApp);

            List <EditableKeywordRecord> onbaseKeyRecord = KeywordUtils.ConvertToOnbaseKeywordRecord(keywordRecords, currentApp);

            long id = unityApiAgent.SaveNewUnityForm(keywordList, UnityFormTemplate, onbaseKeyRecord);

            return id;
        }

        private DocumentType FindDocumentType(DocumentTypeModel documentType)
        {
            DocumentType docType;
            bool documentTypeNamePresent = documentType.Name != null;

            if (documentTypeNamePresent)
            {
                docType = OnbaseDocumentUtils.FindDocumentTypeByName(documentType.Name, _connector.GetCurrentConnection());
            }
            else
            {
                docType = OnbaseDocumentUtils.FindDocumentTypeById(documentType.Id, _connector.GetCurrentConnection());
            }

            return docType;
        }

        private void ValidateDocument(DocumentTypeModel documentType)
        {
            bool documentTypeNamePresent = documentType.Name != null;

            //Validar documentType
            if (documentTypeNamePresent)
            {
                _UnityModelValidator.ValidateDocumentType(documentType);
            }
            else
            {
                _UnityModelValidator.ValidateDocumentTypeById(documentType);
            }
        }
    }
}
