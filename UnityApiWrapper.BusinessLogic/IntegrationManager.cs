﻿using Hyland.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityApiWrapper.BusinessLogic.Contracts;
using UnityApiWrapper.BusinessLogic.Mappers;
using UnityApiWrapper.Models;
using UnityApiWrapper.Models.Enums;
using UnityApiWrapper.Models.Exceptions;
using UnityApiWrapper.Models.Models;

namespace UnityApiWrapper.BusinessLogic
{
    /// <summary>
    /// Es responsable de la interacción entre los clases de lógica de Negocio.
    /// 
    /// </summary>
    /// <remarks>Es necesario cerrar la conexion al terminar</remarks>
    public sealed class IntegrationManager
    {
        private IOnbaseCredentials _onbaseCredentials;
        private OnbaseBL _onbaseAgent;
        private LicenseConnectionType _licenseTypeUsed;

        /// <summary>
        /// Constructor que recibe una implementacion que provea las credenciales de conexion a Onbase
        /// </summary>
        /// <remarks> Este constructor inicia automaticamente una conexion a Onbase. </remarks>
        /// <param name="onbaseCredentials"></param>
        /// <param name="licenseType">Indica el tipo de licencia a utilizar. QueryHour o Named License</param>
        /// <param name="autoInitConnection">Indica si la conexion a onbase se iniciara inmediatamente luego de crear la instancia</param>
        public IntegrationManager(IOnbaseCredentials onbaseCredentials, LicenseConnectionType licenseType, bool autoInitConnection)
        {
            _onbaseCredentials = onbaseCredentials;
            _onbaseAgent = new OnbaseBL(_onbaseCredentials);
            _licenseTypeUsed = licenseType;

            if (autoInitConnection)
            {
                EstablecerConexionSegunTipoLicencia();
            }
        }

        /// <summary>
        /// Se encarga de iniciar una conexion hacia onbase segun el tipo de licencia indicado
        /// </summary>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        public void IniciarConexion()
        {
            EstablecerConexionSegunTipoLicencia();
        }

        private void EstablecerConexionSegunTipoLicencia()
        {
            if (_licenseTypeUsed.Equals(LicenseConnectionType.QUERY_HOUR))
            {
                _onbaseAgent.BeginConnectionWithQueryHour();
            }
            else
            {
                _onbaseAgent.BeginConnectionWithNamedLicense();
            }
        }

        /// <summary>
        /// Devuelve una lista documentos de onbase dado unos criterios de Busqueda.
        /// </summary>
        /// <param name="documentTypesFilters">Tipos de Documentos a buscar</param>
        /// <param name="keywordsFilters">Lista de keywords para realizar el filtrado</param>
        /// <exception cref="InvalidKeywordTypeException"></exception>
        /// <exception cref="InvalidDocumentTypeException"></exception>
        /// <returns>Lista de documentos de onbase</returns>
        public List<OnbaseDocument> BuscarDocumentos(List<DocumentTypeModel> documentTypesFilters, List<KeywordModel> keywordsFilters)
        {
            DocumentList onbaseDocs = _onbaseAgent.GetOnbaseDocuments(documentTypesFilters, keywordsFilters);
            bool anyDocumentWereFound = onbaseDocs.Any();

            if (anyDocumentWereFound)
            {
                List<OnbaseDocument> resultDocuments = ValidationDocumentMapper.ConvertToOnbaseDocumentList(onbaseDocs, _onbaseAgent);
                return resultDocuments;
            }
            else
            {
                return new List<OnbaseDocument>();
            }
        }

        /// <summary>
        /// Devuelve una lista de documentos de consulta dados sus documentHandlers
        /// </summary>
        /// <param name="documentHandlers">Lista de identificadores de documento de Onbase</param>
        /// <exception cref="InvalidKeywordTypeException"></exception>
        /// <exception cref="InvalidDocumentTypeException"></exception>
        /// <returns></returns>
        public List<OnbaseDocument> BuscarDocumentosPorDocumentHandler(List<string> documentHandlers)
        {
            List<Document> documents = _onbaseAgent.GetDocumentsByDocumentHandler(documentHandlers);
            List<OnbaseDocument> docs = new List<OnbaseDocument>();

            if (documents.Any())
            {
                //Agregar codigo de conversion de document a QueryDocument
                docs = ValidationDocumentMapper.ConvertToOnbaseDocumentList(documents, _onbaseAgent);
            }

            return docs;
        }

        /// <summary>
        /// Permite realizar el guardado de documentos en Onbase.
        /// </summary>
        /// <param name="documentType"></param>
        /// <param name="imageInBase64StringFormat">Binario del archivo en formato Base64</param>
        /// <param name="keywords">Lista de keywords a ser almacenados</param>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        /// <returns>Id del documento creado en onbase</returns>
        public string GuardarDocumentos(DocumentTypeModel documentType, string imageInBase64StringFormat, List<KeywordModel> keywords)
        {
            DocumentTypeModel Type = new DocumentTypeModel() { Id = documentType.Id, Name = documentType.Name };

            var documentId = _onbaseAgent.SaveNewDocument(Type, imageInBase64StringFormat, keywords);

            return documentId.ToString();
        }

        /// <summary>
        /// Permite realizar el guardado de UnityForm en Onbase.
        /// </summary>
        /// <param name="documentType">Tipo de documento UnityForm</param>
        /// <param name="keywords">Lista de keywords a ser almacenados</param>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        /// <returns>Id del documento creado en onbase</returns>
        public string GuardarUnityForm(DocumentTypeModel documentType, List<KeywordModel> keywords)
        {
            var documentId = _onbaseAgent.SaveNewUnityForm(documentType, keywords);

            return documentId.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentType">Tipo de documento UnityForm</param>
        /// <param name="keywords">Lista de keywords a ser almacenados</param>
        /// <param name="keywordRecords">Lista de keywordRecords</param>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        /// <returns>Id del documento creado en onbase</returns>
        public string GuardarUnityForm(DocumentTypeModel documentType, List<KeywordModel> keywords, List<KeywordRecordModel> keywordRecords)
        {
            var documentId = _onbaseAgent.SaveNewUnityForm(documentType, keywords, keywordRecords);

            return documentId.ToString();
        }

        /// <summary>
        /// Permite realizar el guardado de un documento en Onbase con paginas.
        /// </summary>
        /// <param name="documentType"></param>
        /// <param name="imagesInBase64StringFormat">binarios de las paginas del documento</param>
        /// <param name="keywords">Lista de keywords a ser almacenados</param>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        /// <returns>Id del documento creado en onbase</returns>
        public string GuardarDocumentoConPaginas(DocumentTypeModel documentType, List<string> imagesInBase64StringFormat, List<KeywordModel> keywords)
        {
            DocumentTypeModel Type = new DocumentTypeModel() { Id = documentType.Id, Name = documentType.Name };

            var documentId = _onbaseAgent.SaveNewDocumentWithPages(Type, imagesInBase64StringFormat, keywords);

            return documentId.ToString();
        }

        /// <summary>
        /// Permite actualizar una lista de keywords de un documento
        /// </summary>
        /// <param name="documentHandler">Identificador del documento</param>
        /// <param name="keywords">Lista de nuevos valores para los keywords</param>
        /// <exception cref="InvalidKeywordTypeException"></exception>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        public void ActualizarKeywords(string documentHandler, List<KeywordModel> keywords)
        {
            _onbaseAgent.UpdateDocumentKeywordList(documentHandler, keywords);
        }

        /// <summary>
        /// Permite eliminar un documento de Onbase dado su documentHandler
        /// </summary>
        /// <param name="documentHandler"></param>
        /// <exception cref="UnityApiUserException"></exception>
        /// <exception cref="UnityApiGeneralException"></exception>
        /// 
        public void EliminarDocumento(long documentHandler)
        {
            _onbaseAgent.DeleteDocument(documentHandler);
        }

        /// <summary>
        /// Permite cerrar la conexion establecida con Onbase
        /// </summary>
        /// <remarks></remarks>
        public void CerrarConexion()
        {
            _onbaseAgent.CloseConnection();
        }
    }
}
